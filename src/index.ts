import { Client, Events, GatewayIntentBits } from "discord.js";
import * as dotenv from "dotenv";
import { registerCommands, handleCommand } from "./commands";
import { log } from "./logger";
import { initializeDatabase } from "./db";
import {
  createFeedItem,
  findFeedItemByUrlAndFeed,
  getAllFeeds,
  incrementFeedsParsed,
} from "./repository/feed.repository";
import Parser from "rss-parser";
import { CronJob } from "cron";
dotenv.config();
const parser = new Parser();

const client = new Client({ intents: [GatewayIntentBits.Guilds] });

client.once(Events.ClientReady, async (c) => {
  log(`Ready! Logged in as ${c.user.tag}`);
  await initializeDatabase();
  await registerCommands();
  checkForNewFeedItems.start();
});

client.on(Events.InteractionCreate, async (interaction) => {
  if (!interaction.isCommand()) return;
  await handleCommand(interaction);
});

const findNewFeedItems = async () => {
  const feeds = await getAllFeeds();
  try {
    log(`Checking ${feeds.length} feeds for new items...`);
    for (const feed of feeds) {
      const data = await parser.parseURL(feed.url);
      for (const item of data.items) {
        if (!item.link) continue;
        const existingItem = await findFeedItemByUrlAndFeed(item.link, feed);
        if (existingItem) continue;

        await createFeedItem(item.link, feed);
        log(`New feed item found: ${item.link}`, "SUCCESS");
        await postFeedItem(item, feed.channel);
        await new Promise((resolve) => setTimeout(resolve, 1000));
      }
      incrementFeedsParsed();
      log(`Finished checking feed: ${feed.url}`, "SUCCESS");
    }
  } catch (error) {
    log(JSON.stringify(error), "ERROR");
  }
};

const postFeedItem = async (
  item: Parser.Item & { [key: string]: string },
  channel: string
) => {
  const foundChannel = client.channels.cache.get(channel);
  if (!foundChannel || !foundChannel.isTextBased()) return;
  await foundChannel.send({
    content: item.comments ? `${item.link}\n\n${item.comments}` : item.link,
  });
  log(`Posted new feed item to channel: ${foundChannel}`, "SUCCESS");
};

export const checkForNewFeedItems = new CronJob(
  process.env.CRON_SCHEDULE as string,
  findNewFeedItems,
  null,
  false
);

client.login(process.env.DISCORD_TOKEN);
