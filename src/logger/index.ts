import pc from "picocolors";

export const log = (
  message: string,
  type: "INFO" | "SUCCESS" | "ERROR" | "WARNING" = "INFO"
) => {
  const timestamp = new Date().toISOString();
  switch (type) {
    case "INFO":
      console.log(pc.blue(`[${timestamp}] [INFO] ${message}`));
      break;
    case "SUCCESS":
      console.log(pc.green(`[${timestamp}] [SUCCESS] ${message}`));
      break;
    case "ERROR":
      console.log(pc.red(`[${timestamp}] [ERROR] ${message}`));
      break;
    case "WARNING":
      console.log(pc.yellow(`[${timestamp}] [WARNING] ${message}`));
      break;
  }
};
