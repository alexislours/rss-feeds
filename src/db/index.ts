import { DataSource } from "typeorm";
import { log } from "../logger";
import { Feed } from "../entities/feed.entity";
import { FeedItem } from "../entities/feed-item.entity";
import { FeedsParsed } from "../entities/feeds-parsed.entity";

export const db = new DataSource({
  type: "better-sqlite3",
  database: "./db/database.sqlite",
  entities: [Feed, FeedItem, FeedsParsed],
  synchronize: true,
});

export const initializeDatabase = async () => {
  await db.initialize();
  log("Database initialized.", "SUCCESS");
};
