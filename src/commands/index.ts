import {
  ChatInputCommandInteraction,
  CommandInteraction,
  REST,
  Routes,
} from "discord.js";
import createFeed from "./create-feed";
import * as dotenv from "dotenv";
import { log } from "../logger";
import info from "./info";
import deleteFeed from "./delete-feed";
dotenv.config();

const commands = [createFeed, info, deleteFeed];
const body = commands.map((c) => c.command.toJSON());
const rest = new REST().setToken(process.env.DISCORD_TOKEN as string);

export const registerCommands = async () => {
  log(`Started refreshing ${commands.length} application (/) commands.`);
  await rest.put(
    Routes.applicationCommands(process.env.DISCORD_CLIENT_ID as string),
    { body }
  );
  log("Refreshed all commands.", "SUCCESS");
};

export const handleCommand = async (interaction: CommandInteraction) => {
  const command = commands.find(
    (c) => c.command.name === interaction.commandName
  );
  if (!command) return;
  await command.handle(interaction as ChatInputCommandInteraction);
};
