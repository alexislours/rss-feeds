import { ChatInputCommandInteraction, SlashCommandBuilder } from "discord.js";
import { countItems, getFeedsParsed } from "../repository/feed.repository";

const command = new SlashCommandBuilder()
  .setName("info")
  .setDescription("Info about the bot.");

const handle = async (interaction: ChatInputCommandInteraction) => {
  const info = await countItems();
  const parsedStats = await getFeedsParsed();
  await interaction.reply({
    content: `Feeds: \`${info.feeds}\`, Items: \`${info.feedItems}\`, Total feeds parsed: \`${parsedStats.feedsParsed}\``,
    ephemeral: true,
  });
};

export default {
  command,
  handle,
};
