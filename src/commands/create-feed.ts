import { ChatInputCommandInteraction, SlashCommandBuilder } from "discord.js";
import { createFeed } from "../repository/feed.repository";

const command = new SlashCommandBuilder()
  .setName("create-feed")
  .setDescription("Add a new feed to the current channel.")
  .addStringOption((option) =>
    option
      .setName("url")
      .setDescription("The URL of the RSS feed.")
      .setRequired(true)
  );

const handle = async (interaction: ChatInputCommandInteraction) => {
  const url = interaction.options.getString("url");
  await createFeed(url as string, interaction.channelId);
  await interaction.reply({ content: "Feed created!", ephemeral: true });
};

export default {
  command,
  handle,
};
