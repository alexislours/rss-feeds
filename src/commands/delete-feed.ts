import { ChatInputCommandInteraction, SlashCommandBuilder } from "discord.js";
import { deleteFeed } from "../repository/feed.repository";

const command = new SlashCommandBuilder()
  .setName("delete-feed")
  .setDescription("Delete a feed from the current channel.")
  .addStringOption((option) =>
    option
      .setName("url")
      .setDescription("The URL of the RSS feed.")
      .setRequired(true)
  );

const handle = async (interaction: ChatInputCommandInteraction) => {
  const url = interaction.options.getString("url");
  await deleteFeed(url as string, interaction.channelId);
  await interaction.reply({ content: "Feed deleted!", ephemeral: true });
};

export default {
  command,
  handle,
};
