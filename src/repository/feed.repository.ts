import { db } from "../db";
import { Feed } from "../entities/feed.entity";
import { FeedItem } from "../entities/feed-item.entity";
import { FeedsParsed } from "../entities/feeds-parsed.entity";

const feedRepository = db.getRepository(Feed);
const feedItemRepository = db.getRepository(FeedItem);
const feedsParsedRepository = db.getRepository(FeedsParsed);

export const createFeed = async (url: string, channel: string) => {
  const feed = new Feed();
  feed.url = url;
  feed.channel = channel;
  await feedRepository.save(feed);
};

export const deleteFeed = async (url: string, channel: string) => {
  const feed = await feedRepository.findOne({
    where: {
      url,
      channel,
    },
  });
  if (!feed) return;
  await feedRepository.remove(feed);
};

export const getAllFeeds = async () => {
  return await feedRepository.find();
};

export const countItems = async () => {
  const feedItems = await feedItemRepository.count();
  const feeds = await feedRepository.count();
  return { feedItems, feeds };
};

export const createFeedItem = async (url: string, feed: Feed) => {
  const item = new FeedItem();
  item.url = url;
  item.feed = feed;
  return await feedItemRepository.save(item);
};

export const findFeedItemByUrl = async (url: string) => {
  return await feedItemRepository.findOne({
    where: {
      url,
    },
  });
};

export const findFeedItemByUrlAndFeed = async (url: string, feed: Feed) => {
  return await feedItemRepository.findOne({
    where: {
      url,
      feed,
    },
  });
};

export const incrementFeedsParsed = async () => {
  const feedsParsed = await feedsParsedRepository.find();
  if (!feedsParsed[0]) {
    const newFeedsParsed = new FeedsParsed();
    newFeedsParsed.feedsParsed = 1;
    await feedsParsedRepository.save(newFeedsParsed);
    return;
  }
  feedsParsed[0].feedsParsed++;
  await feedsParsedRepository.save(feedsParsed[0]);
};

export const getFeedsParsed = async () => {
  const feedsParsed = await feedsParsedRepository.find();
  if (!feedsParsed[0]) return { feedsParsed: 0, itemsParsed: 0 };
  return feedsParsed[0];
};
