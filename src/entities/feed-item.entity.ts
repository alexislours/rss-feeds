import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { Feed } from "./feed.entity";

@Entity()
export class FeedItem {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  url: string;

  @ManyToOne(() => Feed, (feed) => feed.feedItems, {
    cascade: true,
    onDelete: "CASCADE",
  })
  feed: Feed;
}
