import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { FeedItem } from "./feed-item.entity";

@Entity()
export class Feed {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  url: string;

  @Column()
  channel: string;

  @OneToMany(() => FeedItem, (item) => item.feed)
  feedItems: FeedItem[];
}
