import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class FeedsParsed {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  feedsParsed: number;
}
